let tofu1;
var paddleA, ball, wallTop, wallBottom, wallRight;
var MAX_SPEED = 10;
var WALL_THICKNESS = 30;
let score;

function setup() {
  score = createDiv('Score = 0');
  score.position(20, 20);
  score.id = 'score';
  score.style('color', 'blue');

  createCanvas (650,750);
  tofu = new tofu();
  frameRate (100);

paddleA = createSprite(30, height/2, 10, 100);
paddleA.immovable = true;

wallRight = createSprite(width+WALL_THICKNESS/2, height/2, WALL_THICKNESS, height);
wallRight.immovable = true;

wallTop = createSprite(width/2, -30/2, width, 30);
wallTop.immovable = true;

wallBottom = createSprite(width/2, height+30/2, width, 30);
wallBottom.immovable = true;

ball = createSprite(width/2, height/2, 10, 10);
ball.maxSpeed = MAX_SPEED;

paddleA.shapeColor =ball.shapeColor = color(255, 255, 255);

ball.setSpeed(MAX_SPEED, -180);
}


function draw(){
  background(0);
  tofu.move();
  tofu.show();

paddleA.position.y = constrain(mouseY, paddleA.height/2, height-paddleA.height/2);
ball.bounce(wallTop);
ball.bounce(wallBottom);
ball.bounce(wallRight);

var swing;
if(ball.bounce(paddleA)) {
  swing = (ball.position.y-paddleA.position.y)/3;
  ball.setSpeed(MAX_SPEED, ball.getDirection()+swing);
}

if(ball.position.x<0) {
  ball.position.x = width/2;
  ball.position.y = height/2;
  ball.setSpeed(MAX_SPEED, -180);
}

drawSprites();
}


class tofu {
  constructor(){
    this.x = 325;
    this.y = 375;
  }

  move(){
    this.x = this.x + random (-1,1);
    this.y = this.y + random (-1,1);
  }

  show(){
    stroke(255);
    strokeWeight(4);
    noFill();
    rectMode(CENTER);
    rect(this.x, this.y, 642,742);
    }

}

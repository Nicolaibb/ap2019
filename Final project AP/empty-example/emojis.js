function Happy(){
  var happyMouth = ctracker.getCurrentPosition();
    if (happyMouth.length) { //check the availability of web cam tracking
      console.log(happyMouth.length)
      for (let i=44; i<45; i++) {  //loop through the face track point 44 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
        noStroke();
        fill(map(happyMouth[i][0], 0, width, 100, 255));  //color happy mouth with alpha value
        //draw a happy mouth at this position point
        arc(happyMouth[i][0]+35, happyMouth[i][1]-15, 100, 100,0,HALF_PI*2,CHORD);
      }
    }
    var happyLeftEye = ctracker.getCurrentPosition();
      if (happyLeftEye.length) { //check the availability of web cam tracking
        console.log(happyLeftEye.length)
        for (let i=27; i<28; i++) {  //loop through the face track point 27 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
          noStroke();
          fill(map(happyLeftEye[i][0], 0, width, 100, 255));  //color happy left eye with alpha value
          //draw happy left eye with black pupil at this position point
          arc(happyLeftEye[i][0], happyLeftEye[i][1]+10, 50, 70,HALF_PI*2,0,CHORD);
          fill(0);
          ellipse(happyLeftEye[i][0], happyLeftEye[i][1],20,20);
        }
      }
  var happyRightEye = ctracker.getCurrentPosition();
  if (happyRightEye.length) { //check the availability of web cam tracking
    console.log(happyRightEye.length)
    for (let i=32; i<33; i++) {  //loop through the face track point 32 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
      noStroke();
      fill(map(happyRightEye[i][0], 0, width, 100, 255));  //color happy right eye with alpha value
      //draw happy right eye with black pupil at this position point
      arc(happyRightEye[i][0], happyRightEye[i][1]+10, 50, 70,HALF_PI*2,0,CHORD);
      fill(0);
      ellipse(happyRightEye[i][0], happyRightEye[i][1],20,20);
        }
      }
    }

    function Angry(){
      var angryMouth = ctracker.getCurrentPosition();
        if (angryMouth.length) { //check the availability of web cam tracking
        console.log(angryMouth.length)
        for (let i=44; i<45; i++) {  //loop through the face track point 44 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
            fill(map(angryMouth[i][0], 0, width, 100, 255));  //color angry mouth with alpha value
            //draw angry mouth at this position point
            ellipse(angryMouth[i][0]+75, angryMouth[i][1], 50, 50);
            ellipse(angryMouth[i][0], angryMouth[i][1], 50, 50);
            noStroke();
            rect(angryMouth[i][0]+10, angryMouth[i][1]-20, 54, 42);
            fill(0);
            rect(angryMouth[i][0]-25, angryMouth[i][1]-2, 125, 2);

          }
        }
        var angryLeftEye = ctracker.getCurrentPosition();
          if (angryLeftEye.length) { //check the availability of web cam tracking
            console.log(angryLeftEye.length)
            for (let i=27; i<28; i++) {  //loop through the face track point 27 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
              noStroke();
              fill(map(angryLeftEye[i][0], 0, width, 100, 255));  //color angry left eye with alpha value
              //draw angry left eye with black pupil at this position point
              arc(angryLeftEye[i][0], angryLeftEye[i][1], 50, 50,0,PI+QUARTER_PI,CHORD);
              fill(0);
              ellipse(angryLeftEye[i][0]+5, angryLeftEye[i][1]+10,20,20);
            }
          }
    var angryRightEye = ctracker.getCurrentPosition();
      if (angryRightEye.length) { //check the availability of web cam tracking
      console.log(angryRightEye.length)
      for (let i=32; i<33; i++) {  //loop through the face track point 32 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
         noStroke();
         fill(map(angryRightEye[i][0], 0, width, 100, 255));  //color angry rigth eye with alpha value
         //draw angry rigth eye with black pupil at this position point
         arc(angryRightEye[i][0], angryRightEye[i][1], 50, 50,TWO_PI-QUARTER_PI,355,CHORD);
         fill(0);
         ellipse(angryRightEye[i][0]-5, angryRightEye[i][1]+10,20,20);
        }
      }
    }

  function Sad(){
    var sadMouth = ctracker.getCurrentPosition();
      if (sadMouth.length) { //check the availability of web cam tracking
        console.log(sadMouth.length)
        for (let i=44; i<45; i++) {  //loop through the face track point 44 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
           fill(map(sadMouth[i][0], 0, width, 100, 255));  //color sad mouth with alpha value
           //draw sad mouth at this position point
           arc(sadMouth[i][0]+40, sadMouth[i][1]+25, 100, 100,HALF_PI*2,0,CHORD);
        }
      }
      var sadLeftEye = ctracker.getCurrentPosition();
        if (sadLeftEye.length) { //check the availability of web cam tracking
          console.log(sadLeftEye.length)
          for (let i=27; i<28; i++) {  //loop through the face track point 27 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
             noStroke();
             fill(map(sadLeftEye[i][0], 0, width, 100, 255));  //color sad left eye with alpha value
             //draw sad left eye with black pupil at this position point
              arc(sadLeftEye[i][0], sadLeftEye[i][1]-10, 55, 55,PI+QUARTER_PI+1.75, TWO_PI+QUARTER_PI+1.85,CHORD);
              fill(0);
              ellipse(sadLeftEye[i][0]+5, sadLeftEye[i][1]+5,20,20);
           }
         }
  var sadRightEye = ctracker.getCurrentPosition();
    if (sadRightEye.length) { //check the availability of web cam tracking
      console.log(sadRightEye.length)
      for (let i=32; i<33; i++) {  //loop through the face track point 32 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
         noStroke();
         fill(map(sadRightEye[i][0], 0, width, 100, 255));  //color sad right eye with alpha value
         //draw sad right eye with black pupil at this position point
         arc( sadRightEye[i][0], sadRightEye[i][1]-10,55, 55,TWO_PI+QUARTER_PI-0.2, PI+QUARTER_PI, CHORD);
         fill(0);
         ellipse(sadRightEye[i][0]-5, sadRightEye[i][1]+5,20,20);
      }
    }
  }
  function Disgust(){
    var disgustMouth = ctracker.getCurrentPosition();
      if (disgustMouth.length) { //check the availability of web cam tracking
        console.log(disgustMouth.length)
        for (let i=44; i<45; i++) {  //loop through the face track point 44 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
           fill(map(disgustMouth[i][0], 0, width, 100, 255));  //color disgust mouth with alpha value
           //draw disgust mouth at this position point
           rect(disgustMouth[i][0]-10, disgustMouth[i][1]-15, 100, 40,30);

        }
      }
      var disgustLeftEye = ctracker.getCurrentPosition();
        if (disgustLeftEye.length) { //check the availability of web cam tracking
          console.log(disgustLeftEye.length)
          for (let i=27; i<28; i++) {  //loop through the face track point 27 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
             noStroke();
             fill(map(disgustLeftEye[i][0], 0, width, 100, 255));  //color disgust left eye with alpha value
             //draw disgust left eye at this position point
             textStyle(BOLD);
             textSize(90);
             text('>',disgustLeftEye[i][0]-25, disgustLeftEye[i][1]+30);
           }
         }
  var disgustRightEye = ctracker.getCurrentPosition();
    if (disgustRightEye.length) { //check the availability of web cam tracking
      console.log(disgustRightEye.length)
      for (let i=32; i<33; i++) {  //loop through the face track point 32 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
         noStroke();
         fill(map(disgustRightEye[i][0], 0, width, 100, 255));  //color disgust right eye with alpha value
         //draw disgust rigth eye at this position point
         textStyle(BOLD);
         textSize(90);
         text('<',disgustRightEye[i][0]-25, disgustRightEye[i][1]+30);
      }
    }
  }

  function Surprised(){
    var surprisedMouth = ctracker.getCurrentPosition();
      if (surprisedMouth.length) { //check the availability of web cam tracking
        console.log(surprisedMouth.length)
        for (let i=44; i<45; i++) {  //loop through the face track point 44 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
           fill(map(surprisedMouth[i][0], 0, width, 100, 255));  //color surprised mouth with alpha value
           //draw surprised mouth at this position point
           rect(surprisedMouth[i][0], surprisedMouth[i][1]-25, 80, 55,55);

        }
      }
      var surprisedLeftEye = ctracker.getCurrentPosition();
        if (surprisedLeftEye.length) { //check the availability of web cam tracking
          console.log(surprisedLeftEye.length)
          for (let i=27; i<28; i++) {  //loop through the face track point 27 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
             noStroke();
             fill(map(surprisedLeftEye[i][0], 0, width, 100, 255));  //color surprised left eye with alpha value
             //draw surprised left eye with black pupil at this position point
             ellipse(surprisedLeftEye[i][0], surprisedLeftEye[i][1]+5, 50, 60);
             fill(0);
             ellipse(surprisedLeftEye[i][0], surprisedLeftEye[i][1]+10,10,15);
           }
         }
    var surprisedRightEye = ctracker.getCurrentPosition();
    if (surprisedRightEye.length) { //check the availability of web cam tracking
      console.log(surprisedRightEye.length)
      for (let i=32; i<33; i++) {  //loop through the face track point 32 (see: https://www.auduno.com/clmtrackr/docs/reference.html)
         noStroke();
         fill(map(surprisedRightEye[i][0], 0, width, 100, 255));  //color surprised right eye with alpha value
         //draw surprised rigth eye with black pupil at this position point
         ellipse(surprisedRightEye[i][0], surprisedRightEye[i][1]+5, 50,60);
         fill(0);
         ellipse(surprisedRightEye[i][0], surprisedRightEye[i][1]+10,10,15);
      }
    }
  }

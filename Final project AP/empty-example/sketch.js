/* This program has been prepared by Charlotte Jensen, Niels Jacob Tobler and
Nicolai Ahle Bjerg */

var capture;
var ctracker;
var button1;
var button2;
var button3;
var button4;
var button5;
var button6;
var heading;
var s = "SIMPLE IS THE OLD NEW";

function setup() {
  //set up webcam
  var capture = createCapture();
  capture.size(820, 620);
  capture.position(340,120);
  capture.show();
  var c = createCanvas(820, 620);
  c.position(340,120);

  //set up buttons
  button1=createButton('Happy');
  button1.style("padding","5px 45px");
  button1.position(1250,300);
  button1.mousePressed(buttonStateHappy);

  button2=createButton('Angry');
  button2.style("padding","5px 45px");
  button2.position(1250,350);
  button2.mousePressed(buttonStateAngry);

  button3=createButton('Sad');
  button3.style("padding","5px 50px");
  button3.position(1250,400);
  button3.mousePressed(buttonStateSad);

  button4=createButton('Disgust');
  button4.style("padding","5px 40px");
  button4.position(1250,450);
  button4.mousePressed(buttonStateDisgust);

  button5=createButton('Surprised');
  button5.style("padding","5px 35px");
  button5.position(1250,500);
  button5.mousePressed(buttonStateSurprised);

  button6=createButton('Clear');
  button6.style("padding","5px 45px");
  button6.position(1250,550);
  button6.mousePressed(buttonStateClear);

  //set up face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  // set up and style heading
  heading=createElement('p', s);
  heading.position(320,-0);
  heading.style("color", "#000000");
  heading.style("font-size",40+"px");
  heading.style("letter-spacing",".5em");

}

function draw(){
  clear();


  if(happy1){
      Happy();
    }
  if(angry1){
      Angry();
    }
  if(sad1){
      Sad();
    }
  if(disgust1){
      Disgust();
    }
  if(surprised1){
    Surprised();
    }
}

let happy1 = false;

function buttonStateHappy(){
  happy1 = true;
  angry1 = false;
  surprised1 = false;
  sad1 = false;
  disgust1 = false;
  clear1 = false;
}

let angry1 = false;

function buttonStateAngry(){
  angry1 = true;
  happy1 = false;
  surprised1 = false;
  sad1 = false;
  disgust1 = false;
  clear1 = false;
}

let surprised1 = false;

function buttonStateSurprised(){
  surprised1 = true;
  angry1 = false;
  happy1 = false;
  sad1 = false;
  disgust1 = false;
  clear1 = false;
}

let sad1 = false;

function buttonStateSad(){
  sad1 = true;
  happy1 = false;
  angry1 = false;
  surprised1 = false;
  disgust1 = false;
  clear1 = false;
}

let disgust1 = false;

function buttonStateDisgust(){
  disgust1 = true;
  happy1 = false;
  angry1 = false;
  surprised1 = false;
  sad1 = false;
  clear1 = false;
}

let clear1 = false;

function buttonStateClear(){
  clear1 = true;
  angry1 = false;
  happy1 = false;
  surprised1 = false;
  sad1 = false;
  disgust1 = false;
}

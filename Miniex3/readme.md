Miniex 2 - Nicolai Ahle Bjerg - https://cdn.staticaly.com/gl/Nicolaibb/ap2019/raw/master/Miniex3/p5/empty-example/index.html
![screenshot](Skærmbillede.png)

I have made this MiniEx very simple! Maybe also a little bit confusing, but there is a reason why. 
I wanted to make a throbber-icon which is very different from what it is normally. The reason was to make some confusing and make people ask themself if this thing is even loading and if not, what is it then? 
So i made a little square which is just spinning around ifself to eternity. Besides that there is a few lines of text which says 'I am loading', 'or do i?' 'what am i loading?'
The reason why this text is in the program is to make the people who see the project to reflect on the questions and be critical. Why should we wait? does it ever stop? 
But it is also very difficult to see the text, but i think people reflect even more on it, if it hard to see and that is why i made it like that. 

In my MiniEx i used time-related commands as 'framerate()', which i did because i needed to get the right speed of the movement of the square so what it would look like a throbber. 

When i think about my encounters with the throbber-icon it is full of frustration. Every time i see it, it is because i wait for something, which i hate. At the same time i also think back and realise that most of the times i wait for it, it is a waste of time. In many cases it is caused by technical problems, such as lost internet. 
Just take youtube as an example. How often does it come, if it isn't casued by slow internet? It is very rare. 
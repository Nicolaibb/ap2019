function setup() {
 createCanvas(windowWidth, windowHeight);
 frameRate (12);
 textFont("Arial",36);
}

function draw() {
  push();
  fill(0, 0, 0)
  rect(0, 0, width, height);
  drawThrobber(20);
  pop();
  text('i am loading', 905, 530);
  text(' or am i?', 300, 200)
  text('what am i loading?', 1000, 100)
  fill(220, 220, 100, 20);
}

function drawThrobber(num) {
  push();
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(220,220,220);
  square(10,0,50);
  pop();
}

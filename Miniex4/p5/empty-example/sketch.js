var capture;
var button;
var ctracker;

function setup() {
 createCanvas(windowWidth, windowHeight);
 background (100);
 textFont("Arial",36)

  // WEB CAM capture
 capture = createCapture();
 capture.size(640,480);
 capture.position(750,80);
 capture.hide();

  //knappen
 button = createButton('CLICK ME FOR PROVE');
 button.position(1000, 625);

 //face tracker
 ctracker = new clm.tracker();
 ctracker.init(pModel);
 ctracker.start(capture.elt);
}

//button 1
function knap (){
  capture.show();
text ("MOVE YOUR HEAD",150, 590);

}

function draw(){
  push();
  button.mousePressed(knap)
  text ("I SEE YOUR EYES",900,100)
  text ("DONT BELIEVE ME?",900, 590)
  line(650,0,650,900)
  pop();

//facetracker
  var positions = ctracker.getCurrentPosition();
    if (positions.length) { //check the availability of web cam tracking
      for (let i=0; i<positions.length; i++) {  //loop through all major face track points (see: https://www.auduno.com/clmtrackr/docs/reference.html)
         noStroke();
         fill(map(positions[i][0], 0, width, 100, 255), 0,0,10);  //color with alpha value
         //draw ellipse at each position point
         ellipse(positions[i][0], positions[i][1], 5, 5);
      }
    }
}

MiniEx 4 - Nicolai Ahle bjerg - https://cdn.staticaly.com/gl/Nicolaibb/ap2019/raw/master/Miniex4/p5/empty-example/index.html
![screenshot](skærmbillede.png)

This MiniEx4 has focus on data capturing. My program is a grey screen with one button. When you press the button a screen of live facecam appers. On the left side you can see a face made by red dots which moves depending on how you move your head. This is made by help of the face tracker. 

I found the coding process towards getting this result very challenging. First of all i had a problem with not showing the camera from start. But i solved the problem with the hide(); function. 
After that i wanted the camera to appear when i pressed the mouse, so i used the mousePressed (); function. But the problem here is that this function only runs once when the mouse is pressed. Then i knew that i had to in someway get the function into the draw function. I also decided to make a button instead and that is why the coding is as it is. 

I actually think the result of this program is quite good. The thoughts behind it is to make people think about data capturing and what it is cabable of. In relation to this it also focus on the discussion of surveillance. Do we know how much the computer is cabable of? If i, as a student, can make a program within a day, which can see my face in details without the person knows it. Where is the limit then? 
For me, it is a terrifeing fact that so much is possible. And all this goes back the data capturing, which is why it is such an important subject. 
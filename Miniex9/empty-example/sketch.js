/*
class09: Fetching and visualizing images from Google image search API
- ok to run on firefox, but if you run on Chrome you need to install extra add on: Allow-Control-Allow-Original:* (https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en)
- credit: Image Lines in Processing by Anna the Crow https://www.openprocessing.org/sketch/120964
- full url here: https://www.googleapis.com/customsearch/v1?key=???&cx=????&imgSize=small&q=warhol+flowers
*/
var url = "https://www.googleapis.com/customsearch/v1?";
var apikey = "AIzaSyDL9fAoq99knG9E7bRXHGyCF2Dcy-mW_i0";//"INPUT YOUR OWN KEY";  //register API key here: https://developers.google.com/custom-search/json-api/v1/overview
var engineID = "017200131888597510095:pw4kigrmkww";// "INPUT YOUR OWN"; //https://cse.google.com/all  | create search engine, then get the searchengine ID - make sure image is on

var query = "dick";  //search keywords
var imgSize ="large"; //small, check here: https://developers.google.com/custom-search/json-api/v1/reference/cse/list#parameters
var request; //full API

var img;
var getImg;
var loc;
var img_x, img_y;
var img2;
var img3;

function preload() {

img2=loadImage("dick.jpg");
img3=loadImage("dick2.jpg");

}

function setup() {
	createCanvas(windowWidth,windowHeight);
	background(255);
	frameRate(10);
	fetchImage();


  image(img2,200,200,300,300);
  image(img3,200,60,810,124);

  textStyle(BOLD);
  text("NORMAL GOOGLE SEARCH",280,500);
  text("GOOGLE API SEARCH",820,500);
  textSize(25);
  text("HOW MUCH CONTROL DO WE REALLY HAVE?",360,600);

  noFill();
  stroke(255,0,0);
  strokeWeight(4);
  rect(50,50,1150,500)

}

function centerCanvas() {
	let x = (windowWidth - width) / 2;
  let y = (windowHeight - height) / 2;

}
function gotData(data) {   //a callback needs an argument
	//getImg = data.items[3].pagemap.cse_image[0].src;  //items responsible for which image
	getImg = data.items[0].pagemap.cse_thumbnail[0].src;  // this is the thumbnail
	//getImg = data.items[0].pagemap.cse_image[0].src; //full image
	console.log(getImg);
}

function draw() {
	try {	//takes time to load the external image, that's why you see errors in the console.log

		loadImage(getImg, function(img) {
		push();
		translate(width/2-img.width/2, 0);
		image(img,250,280);

		 //try to uncomment this block if you manage to get the image.
/*
		img.loadPixels();
		img_x = floor(random(0,img.width));
		img_y = floor(random(0,img.height));
		loc = (img_x+img_y * img.width)*4; // formular to locate the no: x+y*width, indicating which pixel of the image in a grid (and each pixel array holds red, green, blue and alpha values - 4) can see more here: https://www.youtube.com/watch?v=nMUMZ5YRxHI
		stroke(color(img.pixels[loc],img.pixels[loc + 1], img.pixels[loc+2]));  //rgb values
		line(img_x,0,img_x,height);
		*/

		pop();
		});
	}catch(error) {
  		console.error(error);
	}
}

function fetchImage() {
	request = url + "key=" + apikey + "&cx=" + engineID + "&imgSize=" + imgSize + "&q=" + query;
	console.log(request);
	loadJSON(request, gotData); //this is the key syntax and line of code to make a query request and get a query response

}

MiniEx 5 - Nicolai Ahle bjerg - https://cdn.staticaly.com/gl/Nicolaibb/ap2019/raw/master/Miniex%205/p5/empty-example/index.html
![screenshot](skærmbillede.png)

So this Miniex i used for some kind of training and a way to learn new syntaxes. There is no deeper meaning with this program, but more about understanding and learning every single thing in the functions i used.
I decided to try to upload image which i could use in the program. I started to check out the references on p5.js and i found out that i could use both createImage() and loadImage(). So i tried both out and i acutally had a very hard time getting it to work because i mixed to things together. But i got there. 
I also knew that i had to use the preload() command. Because i want to show the image from the very beginning and the program should not start before the image is loaded. And that is why my code starts with that. 
The next thing i wanted to explore was to have something moving on the screen in the same movement forever. So i decided to create a rect which were going from one side of the screen and to the other side and goes back and so on. 
I placed in the middle and i had to use the if command so that when the rect reach 1580 on the x cordinate it goes back to -300. This if command is what makes the rect move forever. 

I also changed the speed of the rect many times with the purpose of getting what i think looked like the most fluent movement without being to fast. I did this by 'rect1=rect1+5;' changing the 5 in this sentence, which make a change in the speed.

The reason why i choose to use the loadImage() commmand to get the image on the screen was that in that way i succeeded in making it 'behind' the rect. When i used the createImage() command it was always in front of the rect. 
I tried to figure out why by reading the p5 references but i must admit that i didn't succeed. 

The thoughts behind this project was almost none existing (because i saw this week as training). The only thing i really thought a deeper meaning about was the picture which shows a eternity symbol and at the same time the rect goes on in eternity.



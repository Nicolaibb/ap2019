MiniEx 5 - Nicolai Ahle bjerg - https://cdn.staticaly.com/gl/Nicolaibb/ap2019/raw/master/Miniex7/p5/empty-example/index.html
![screenshot](skærmbillede.png)

This Runme was made by a group of 3, but this readme is just by me. This readme will focus on the rules that we implemented in the program. First i will list all of the rules down.

Rule 1: Set the first rectangle in Africa. The program has to start somewhere and we made a rule which has the purpose to start the program in the same way every single time. 

Rule 2: Generate a random size, x position and y position for the new rectangle. The program generate new rectangles all the time and to do that we made a rule which decided the position and size of it. 

Rule 3: Check which rectangle is the closest to the current rectangle. Every time the program generates a new rectangle it is always 'in line' or very close to the one genereted just before. This is also done by one of our rules and it makes the rects generated in a way that dosn't look like it is random. 

Rule 4: Show the original position of the rectangle in the shape of a circle and create a line to the new position that represent the angle. As you can see in the program there is a lot of lines where all of them have a circle at the end, but they only shows for a short moment. This happens every time that a new rect is generated. The line shows from where the old rect was and in which way the new one will be generated. 

Rule 6: Give the rectangle a random color within the green spectrum. No rect is similair to eachother in this program and this is also valid in relation to color. Every rect has a new color, but within the limits of the green spectrum.

Rule 7: Repeat until the max count of the rectangles is reached. The program actually has a stopping point. When it has created 5000 rects it will stop. Both because it won't be visible anymore because it is out of the screen, but also to avoid a crash.


What does all these rules do to our program? For us all these rules are a way to generate the outcome that we have been thinking of. Without these rules the coding would have been very different. I also see it as a way to make it 'our' program and not the computers. 
We made all these rules to tell the computer what we want and which rules it has to operate under. And in this program it has been a succes. So i think the authorship is ours and not the computers, because the computer only does what we tells it to do. 

In relation to the loops which we use by always generate new rects the loops make the program way more interesting, because you always try to follow which direction the rect are generated. This can be difficult though, because it generates in some way random. 
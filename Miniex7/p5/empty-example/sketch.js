'use strict';

var maxCount = 5000; // max count of the cirlces
var currentCount = 1;
var x = [];
var y = [];
var r = [];
var randomColor;
var img;

function preload(){
img = loadImage("kort.jpg")
}

function setup() {
  textFont("impact",)
  imageMode(CORNER);
  createCanvas(1400, 800);
  strokeWeight(1);
  frameRate(40);


  // first square
  x[0] = width / 1.8;
  y[0] = height / 2;
  r[0] = 10;
}

function draw() {
  clear();
  image (img, 0,0,1400,800);
  push();
  textSize(40);
  text("SURVEILLANCE",620,80)
  noFill();
  strokeWeight(5)
  rect(40,10,1345,730)
  pop();

  // create a random set of parameters
  var newR = random(3, 7);
  var newX = random(newR, width - newR);
  var newY = random(newR, height - newR);

  var closestDist = Number.MAX_VALUE;
  var closestIndex = 10;

  // which square is the closest?
  for (var i = 0; i < currentCount; i++) {
    var newDist = dist(newX, newY, x[i], y[i]);
    if (newDist < closestDist) {
      closestDist = newDist;
      closestIndex = i;
    }
  }

  // show original position of the sqaure and a line to the new position
  fill(20);
  ellipse(newX, newY, newR * 2, newR * 2);
  line(newX, newY, x[closestIndex], y[closestIndex]);

  // aline it to the closest circle outline
  var angle = atan2(newY - y[closestIndex], newX - x[closestIndex]);

  x[currentCount] = x[closestIndex] + cos(angle) * (r[closestIndex] + newR);
  y[currentCount] = y[closestIndex] + sin(angle) * (r[closestIndex] + newR);
  r[currentCount] = newR;
  currentCount++;

  // draw them
  for (var i = 0; i < currentCount; i++) {
  randomColor = color(random(0),random(255),random(0));
	fill(randomColor);
    rect(x[i], y[i], r[i] * 2, r[i] * 2);
  }

  if (currentCount >= maxCount) noLoop();
}

function keyReleased() {
  if (key == 's' || key == 'S') saveCanvas(gd.timestamp(), 'png');
}

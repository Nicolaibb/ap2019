function setup() {
  createCanvas(1450, 750, WEBGL);
}

function draw() {
  background(70, 70, 70);
  translate(0, -75, 0)
  push();
  rotateX(frameCount * 0.00);
  rotateY(frameCount * 0.03);
  fill(100, 250, 250);
  box(100,200,24,24);

  translate(240, 0, 0);
  push();
  rotateX(frameCount * 0.03);
  rotateY(frameCount * 0.00);
  fill(100, 200, 50);
  box(100,200,24,24);

  translate(240, 0, 0);
  push();
  rotateX(frameCount * 0.04);
  rotateY(frameCount * 0.00);
  fill(100, 150, 200);
  box(100,200,24,24);

  translate(240, 0, 0);
  push();
  rotateX(frameCount * 0.05);
  rotateY(frameCount * 0.00);
  fill(100, 30, 270);
  box(100,200,24,24);

  translate(240, 0, 0);
  push();
  rotateX(frameCount * 0.07);
  rotateY(frameCount * 0.00);
  fill(250, 75, 150);
  box(100,200,24,24);
}

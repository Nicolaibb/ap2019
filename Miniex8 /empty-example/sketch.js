let sos;
let helpInSight=20; //ypos
let signal = 500; //xpos



function preload() {
  sos=loadJSON('stranded.json');

}

function setup() {
  createCanvas(windowWidth, windowHeight);
  textSize(30);

}

function draw() {
  background(0);

  for(var i=0; i<sos.morsecode.length;i++){
    if (i < sos.morsecode.length/2) {
      signal = signal + 20;
    } else {
      signal = signal - 20;
    }
    fill(255);
    stroke(0,255,0,200);
    strokeWeight(2);

    text(sos.morsecode[i].iamhere,signal-120,150+helpInSight*i);
  }

  for(var j=0; j<sos.morsecode.length;j++){
    if (j < sos.morsecode.length/2) {
      signal = signal - 20;
    } else {
      signal = signal + 20;
    }
    fill(255);
    stroke(0,255,0,200);
    strokeWeight(2);

    text(sos.morsecode[j].iamhere,400+signal,150+helpInSight*j);
  }
}

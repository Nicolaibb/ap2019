var lefteye = ["0", "o","Q"];
var righteye = ["0", "o","Q"]
var mund = ["_", "O","U","V","--"]

function setup() {
  createCanvas(1450, 775);
  textFont("Arial",36);
  noLoop();
  frameRate(10);
}

function mousePressed() {
strokeWeight(1);
fill(200,200,100)
var randomword = random(righteye);
text(randomword, mouseX-30,mouseY-50);

var randomword = random(lefteye);
text(randomword, mouseX+30, mouseY-50);

var randomword = random(mund);
text(randomword, mouseX, mouseY);

noFill();
stroke(200,200,100);
strokeWeight(4);
circle(mouseX+7, mouseY-40, 70);
}

function draw() {
  background(31);

  rectMode(CENTER);
  fill(100);
  stroke(200,200,100);
  strokeWeight(4);
  rect(725,375,800,400,30)

  fill(200,200,100)
  noStroke();
  text('press mouse to generate REAL emoji', 420, 300);
  text('WHO ARE YOU?', 580, 450);

  line(mouseX, mouseY, pmouseX, pmouseY);
  print(pmouseX + ' -> ' + mouseX);


}
